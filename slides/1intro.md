# Practical FP 
(in Java)

---slide---

## whoami
<img style="float: right;" src="slides/images/photo.jpg">

* Ties van de Ven
* Developer for 11 years
* Clean code
* Why vs How

---slide---

## What is this talk NOT about
* Functional Programming
* Java

---slide---

## What is this talk about
* Root cause analysis
* Identifying problems you didn't know you had
* Ways to reason about code
* Cleaner code

---slide---

## So why is this talked named Practical FP in Java..
* Most concepts are actively used in FP
* These concepts are not bound to a particular language

---slide---

## Agenda
Fp.. the practical side

* What is FP
* Make state changes explicit (Immutability)
* Make code flow explicit (Streams)
* Honest functions (Monads)
* Trustworthy functions (Pure functions)