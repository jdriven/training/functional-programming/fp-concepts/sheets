## Streaming API

---slide---

### Data operations
* map
* filter
* reduce

---slide---

### Map
Given an element, transform it to another element

A &rightarrow; B

---slide---

### Filter
Given an element, remove it, or not

A &rightarrow; A?

---slide---

### Reduce
Collect all elements and reduce it to 1 value

(that value can also be a new list)

---slide---

### Building blocks
 <img src="slides/images/blocks.png">

---slide---

### In practice
```java
List.of("meterId").stream()
	.filter(meterId -> isAllowed(meterId))  //filter
	.map(meterId -> toMeter(meterId))       //map
    .flatMap(meter -> toValues(meter))      //map (and flatten)
	.collect(Collectors.toList())           //reduce
```

---slide---

### Thinking in types
Person -> ValidatedPerson
```java
public void foo(ValidatedPerson p){ ... }
```

---slide---

### Homework
* Identify the building blocks
* Identify your types
* Chain them using streams
* Treat yourself for using java 8