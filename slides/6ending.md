## Conclusion
* State is dangerous &rightarrow; Consider Immutability
* Functions should be honest &rightarrow; Consider Optional
* Functions should be trustworthy &rightarrow; Consider separating side-effects
* Clean up your code by using these principles
* Vavr (also, you dont need a framework)
---slide---


## Keep in touch
* ties_ven @Twitter
* Ties van de Ven @LinkedIn
* Or talk to me in real life