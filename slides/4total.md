## Honest functions
__***FUNCTION***__al programming
<!-- lets talk about functions.. -->

---slide---

### Function signatures

```java
public String foo(int bar) // int -> String
```

---slide---

## We care for them
```java
public ArrayList&lt;String> foo(ArrayList&lt;String> bar) //Whats wrong here?
```

---slide---

```java
public List&lt;String> foo(List&lt;String> bar) //Much more liberal
```

---slide---

### Checked exceptions
```java
public double divide(double a, double b) throws ArithmeticException
```
```java
try {
  divide(1,2);
} catch(ArithmeticException e){
  //Wish there was a -XXCompilerTrustMeOnThisOne mode
}
```

---slide---

### Checked exception alternatives
```java
/**
* Will throw an ArithmeticException if b equals 0
*/
public double divide(double a, double b){
	throw new ArithmeticException(); // Something went wrong, kill current thread
}
```
```java
/**
* Will return null if b equals 0
*/
public Double divide(double a, double b){
	return null; // nullpointer incoming
}
```
---slide---

### Optional

```java
public Optional&lt;Double> divide(double a, double b){
	if(b == 0){
		return Optional.empty();
	}
	return Optional.of(a / b);
}

Double result = divide(1, 0).orElse(0)
```
---slide---

### Optional vs Checked exceptions

* Both force the programmer to deal with an invalid state during compile time
* Allows the caller of the function to deal with the fault situation
* Optional has a much friendlier API

---slide---

### Homework

* Instead of throwing an exception/return null, return an Optional
* Treat yourself for using a __MONAD__ to make the function __TOTAL__

---slide---

### Monad paradox
> "Once you understand monads, you lose the ability to explain them to others"

---slide---

### Monad
* A monad is a wrapper around a value to use as a return type to make fault states explicit
* Java only knows Optional...

---slide---

### Total function

* A total function has a valid output to every input
* Fault situations can give back a monad instead of an exception
* Forces programmers to deal with fault scenarios (with a friendly API)

---slide---

### Monad pattern

Componse application flow

```java
public Optional&lt;String> getName();
public Optional&lt;Person> findPerson(String name);
public Optional&lt;Order> findOrder(Person person);
public int getAmount(Order order);

int amount = getName().flatMap(name -> findPerson(name))
					  .flatMap(person -> findOrder(person))
					  .map(order -> getAmount(order))
					  .orElse(0)
```