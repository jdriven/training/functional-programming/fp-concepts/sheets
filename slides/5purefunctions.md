## Trustworthy functions
---slide---

### What makes a function untrustworthy
* Changing state outside of the function
* Using mutable state from outside the function
* Does something that may cause an exception
---slide---

### Why
* Function behaviour is no longer idempotent
* Function order starts to matter
* Have to deal with a bad flow
---slide---

### Homework
* Don't use mutable state from outside function scope, don't mutate state outside function scope
* Treat yourself for making a function __REFERENTIAL__ __TRANSPARENT__
---slide---

## Pure functions
A function can be considered pure if it is
* Idempotent (given an A, always produce a B)
* Total (For every input, produce a valid output)
* Referential transparent (don't user alter state outside of the function)
---slide---

## Pure functions
This makes them
* Reliable
* Testable
---slide---

### Side-effects
Are things that make a function not pure. examples are
* Setters
* I/O (accessing a file/database/webservice)
* Logging
---slide---

### But we need those things...
---slide---

## Dealing with side-effects
<img src="slides/images/orchistrator.png">
---slide---

### In code
```java
public void foo(int id){ //hard to test
    User user = findUserInDatabase(id); //side-effect
    //logic
    if(user.name.startsWith("A")){  
        user.setAllowed(true);
    } else {
        user.setAllowed(false);
    }
    storeUserInDatabase(user); //side-effect
}
```
---slide---

### In code 2
```java
public void foo(int id) {
    User user = findUserInDatabase(id); //side-effect
    User modifiedUser = doBusinessLogic(user); //pure-function
    storeUserInDatabase(modifiedUser); //side-effect
}
public User doBusinessLogic(User user) { //now very testable
    if(user.name.startsWith("A")){
        return new User(user.name, true);
    } 
    return new User(user.name, false);
}
```
---slide---

### To make our functions pure
* Identify the side-effects in your application
* Put these into seperate functions/components
* Use the orchistrator pattern to seperate the side-effects from the pure code