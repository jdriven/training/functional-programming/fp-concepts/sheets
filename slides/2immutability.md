## What is FP
<img style="float: right; width:40%; height:40%" img src="slides/images/wordcloud.png">

* Looks quite complex
* Based on category theory
<!-- ik dacht dat ik kon programmeren... -->

---slide---

### Practice
* Basically just good code practices
* Making things more explicit
 * Helps with reasoning 
   * In an abstract way 
      * Good for problem solving

---slide---

### FP vs OO
* OO models code based on the real world
* FP models code based on math (it wants an __DETERMINISTIC__ application)

---slide---

### Idempotent
A &rightarrow; B

---slide---

### Idempotent in practice
```java
public int foo(int a){ return a + 1; } //Idempotent

public ZonedDateTime bar() { return ZonedDateTime.now(); }  //Not idempotent
```
---slide---

### Cars examples
Blue car &rightarrow; Red car

---slide---

### Car
```java
public class Car {
    private String color;
}
```

---slide---

### State is dangerous
A lot of problems are related to state...
* Nullpointers
* Code breaking in a different place after a fix
* Race conditions

---slide---

### How to deal with state
* Avoid state (pass as argument)
* Make sure it's consistent (immutable)
* Mutable

---slide---

### Scoping
```java
int var3? //Very dangerous

public class Foo {

	private int var2; //Dangerous
	
	public void foo(){
		int var1; //Safe
	}
}
```

---slide---

### IlLegal state
Examples are
* Null
* Incorrect value (the color property has the value `coffee machine`)
<!-- kleur kiezen is lastig gezien de grote hoeveelheden rare kleuren -->

---slide---

### Keeping a legal state
* Create objects in a legal state (constructor)
* Remove ways to change the state (setters)

---slide---

### How to change a value
No setters... so constructor
```java
public void foo(Person p){
    Person p2 = new Person(p.getName(), p.getAge() + 1);
}
```
---slide---

## Performance
<img src="slides/images/all_the_objects.jpg">

---slide---

### Homework
* Add an all-arg constructor
* Create objects in a legal state
* Don't use setters
* Treat yourself for using __IMMUTABILITY__

---slide---

### Immutability pattern 1
explicit state-changes
```java
public void foo(Person p){
    System.out.println(p);
    doSomething(p);
    System.out.println(p);
}

public void foo(Person p){
    System.out.println(p);
    Person p2 = doSomething(p);
    System.out.println(p2); //input Person is not changed!
}
```
---slide---

### Immutability pattern 2
No more setter hell
```java
public void creatingPersonInIllegalState(){
    Person p = new Person(); //No compilation error if we add more properties
    p.setName("Arthur"); // Step 1 in bringing the person object into a correct state
    additionalSetterCallsElsewhere(p);
	doSomething(p) //Might not work if previous function is altered
}

public void additionalSetterCallsElsewhere(Person p){
    p.setAge(42); // Step 2 in bringing the person object into a correct state
}
```
---slide---

### Immutability pattern 3
Forced order
```java
public void collectInfoAndCreatePerson(){
    String name = retrieveName();
    if(name == null){ throw new IllegalStateException() } //Exceptions are evil too
    int age = retrieveAge();
    Person p = new Person(name, age); //Creating in in the correct state
}
```